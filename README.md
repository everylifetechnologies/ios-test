**ELT iOS Interview task**

*Requirements: Xcode 10.1*

*You are given the outline of an app that displays a list of tasks. These tasks come in four types*
- General
- Medication
- Hydration
- Nutrition

*Your task is to complete the app so that it displays the tasks as shown in 'design.png' (the icons are included in Assets.xcassets), and make it possible for the user to filter the task list using the buttons at the bottom of the screen. You will also need to finish off the half finished unit tests and add any new ones that you think are relevant.*

The app currently displays an empty table view that is stuck with a loading animation, and a set of buttons that will be used for filtering the table once it has been populated.

There are two sets of TODO's that must be completed to restore it's functionality. 

TODO A's should be done first. These tasks are centered around creating the missing UI and returning a full list of the tasks.

Once this is complete you can then complete TODO B's, should are focused on adding the filtering functionality to the app.
 
Feel free to create any additional classes/files you think are needed.
