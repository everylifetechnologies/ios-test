//
//  TasksViewModel.swift
//  TechnicalTest
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import UIKit

protocol TasksViewModelDelegate: class {
    func reloadTable()
    func beginRefreshing()
    func endRefreshing()
}

class TasksViewModel {
    
    private let tasksService: TasksServicing
    
    weak var delegate: TasksViewModelDelegate?
    
    private var taskItemViewModels = [TaskItemViewModel]()
    
    init(tasksService: TasksServicing) {
        self.tasksService = tasksService
    }
    
    func refreshTasks() {
        self.delegate?.beginRefreshing()
        
        self.tasksService.allTasks { (tasks) in
            self.delegate?.endRefreshing()
            
            self.taskItemViewModels = tasks.map({ TaskItemViewModel(task: $0) })
            self.delegate?.reloadTable()
        }
    }
    
    func filterTasks(taskTypes: [TaskType]) {
        // TODO B: implement this so that the table in TasksViewController will only show the tasks contained in 'taskTypes' when this method is called.
        // You may wish to reengineer filteredTaskItems() to help with this
    }
    
    func filteredTaskItems() -> [TaskItemViewModel] {
        return self.taskItemViewModels
    }
}

struct TaskItemViewModel {
    
    let id: Int
    let name: String
    let description: String
    let type: TaskType
    let typeIcon: UIImage
    
    init(task: Task) {
        self.id = task.id
        self.name = task.name
        self.description = task.description
        self.type = task.type
        switch task.type {
        case .general:
            self.typeIcon = #imageLiteral(resourceName: "general")
        case .hydration:
            self.typeIcon = #imageLiteral(resourceName: "hydration")
        case .medication:
            self.typeIcon = #imageLiteral(resourceName: "medication")
        case .nutrition:
            self.typeIcon = #imageLiteral(resourceName: "nutrition")
        }
    }
}
