//
//  TasksViewController.swift
//  TechnicalTest
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var generalFilterButton: UIButton!
    @IBOutlet var medicationFilterButton: UIButton!
    @IBOutlet var hydrationFilterButton: UIButton!
    @IBOutlet var nutritionFilterButton: UIButton!
    
    private var viewModel: TasksViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tasksService = TasksService()
        self.viewModel = TasksViewModel(tasksService: tasksService)
        self.viewModel.delegate = self
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        
        self.refreshData()
    }
    
    @objc func refreshData() {
        self.viewModel.refreshTasks()
    }
    
    @IBAction func toggleFilter(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.alpha = 1
        } else {
            sender.alpha = 0.4
        }
        var filteredTaskTypes = [TaskType]()
        if self.generalFilterButton.isSelected {
            filteredTaskTypes.append(.general)
        }
        if self.medicationFilterButton.isSelected {
            filteredTaskTypes.append(.medication)
        }
        if self.hydrationFilterButton.isSelected {
            filteredTaskTypes.append(.hydration)
        }
        if self.nutritionFilterButton.isSelected {
            filteredTaskTypes.append(.nutrition)
        }
        self.viewModel.filterTasks(taskTypes: filteredTaskTypes)
    }
}

extension TasksViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.filteredTaskItems().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO A: implement this method so that TasksTableViewCells that show the correct filteredTaskItems are returned
        return UITableViewCell()
    }
}

extension TasksViewController: TasksViewModelDelegate {
    
    func reloadTable() {
        self.tableView.reloadData()
    }
    
    func beginRefreshing() {
        self.tableView.refreshControl?.beginRefreshing()
    }
    
    func endRefreshing() {
        self.tableView.refreshControl?.endRefreshing()
        self.tableView.refreshControl = nil
    }
}

