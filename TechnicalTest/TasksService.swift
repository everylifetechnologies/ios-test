//
//  TasksService.swift
//  TechnicalTest
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import Foundation

protocol TasksServicing {
    
    func allTasks(completion: @escaping ([Task]) -> ())
}

class TasksService: TasksServicing {
    
    func allTasks(completion: @escaping ([Task]) -> ()) {        
        // TODO A: Implement this with the help of TasksApiService
    }
}
