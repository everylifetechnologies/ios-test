//
//  TasksViewModelTests.swift
//  TechnicalTestTests
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import XCTest
@testable import TechnicalTest

class TasksViewModelTests: XCTestCase {

    func testRefreshTasksWithOnlyOneTaskWillOnlyReturnOneFilteredTask() {
        
        // Given
        let task = Task(id: 1, name: "some name", description: "some description", type: .general)
        let tasksService = MockTasksService(tasks: [task])
        let viewModel = TasksViewModel(tasksService: tasksService)
        
        // When
        viewModel.refreshTasks()
        
        // Then
        XCTAssertEqual(1, viewModel.filteredTaskItems().count)
        XCTAssertEqual(1, viewModel.filteredTaskItems()[0].id)
        XCTAssertEqual("some name", viewModel.filteredTaskItems()[0].name)
        XCTAssertEqual("some description", viewModel.filteredTaskItems().description)
    }
    
    func testFilterTasksByGeneralWillOnlyReturnGeneralTasks() {
        // TODO B: Implement this test
    }
    
    // TODO B: Add any other relevant tests
    
    private class MockTasksService: TasksServicing {
        
        private let tasks: [Task]
        
        init(tasks: [Task]) {
            self.tasks = tasks
        }
        
        func allTasks(completion: @escaping ([Task]) -> ()) {
            completion(self.tasks)
        }
    }
}
